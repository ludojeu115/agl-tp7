package fr.umds.Ter_organizer;

/**
 * 
 */
public class Creneau {

    /**
     * Default constructor
     */
    public Creneau() {
    }

    /**
     * 
     */
    private Sujet sujet_;

    /**
     * 
     */
    private String start_;

    /**
     * 
     */
    private String end_;

    /**
     * parameterized constructor
     * 
     * @param sujet
     * @param start
     * @param end
     */
    public Creneau(Sujet sujet, String start, String end) {
        sujet_ = sujet;
        start_ = start;
        end_ = end;
    }

    /**
     * 
     * @return sujet
     */
    public Sujet getS() {
        return sujet_;
    }

    /**
     * 
     * @return start
     */
    public String getSt() {
        return start_;
    }

    /**
     * 
     * @return end
     */
    public String getE() {
        return end_;
    }

}