package fr.umds.Ter_organizer;

/**
 * 
 */
public class Etudiant {

    /**
     * Default constructor
     */
    public Etudiant() {
    }


	/**
     * 
     */
    private Integer iNE_;

    /**
     * 
     */
    private String nom_;

    /**
     * 
     */
    private String prenom_;

    /**
     * 
     * @param iNE
     * @param nom
     * @param prenom
     */
    public Etudiant(Integer iNE, String nom, String prenom) {
        iNE_=iNE;
        nom_=nom;
        prenom_=prenom;
    }

    /**
     * 
     * @return
     */
    public Integer getI() {
        return iNE_;
    }

    /**
     * 
     * @return
     */
    public String getN() {
        return nom_;
    }

    /**
     * 
     * @return
     */
    public String getP() {
		return prenom_;
	}

}