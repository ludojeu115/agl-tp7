package fr.umds.Ter_organizer;

import java.util.*;

/**
 * 
 */
public class Planning {

    /**
     * Default constructor
     */
    public Planning() {
    }

    /**
     * 
     */
    private List<Creneau> creneaux_;

    /**
     * 
     * @param sj
     */
    public Planning(List<Sujet> sj) {
        for (Sujet s : sj) {
            String end = "1";
            String start = "0";
            for (Creneau cr : creneaux_) {
                if (s.getE().getI().equals(cr.getS().getE().getI())
                        && Integer.parseInt(end) < Integer.parseInt(cr.getE())) {
                    start = cr.getE();
                    end = Integer.toString(Integer.parseInt(cr.getE()) + 1);

                }
            }

            Creneau cr = new Creneau(s, start, end);
            creneaux_.add(cr);
        }
    }

    /**
     * 
     */
    public void save() {
        // TODO implement here
    }

    /**
     * 
     */
    public void createICal() {
        // TODO implement here
    }

}