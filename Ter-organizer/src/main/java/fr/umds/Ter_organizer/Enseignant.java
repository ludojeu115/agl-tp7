package fr.umds.Ter_organizer;

/**
 * 
 */
public class Enseignant {

    /**
     * Default constructor
     */
    public Enseignant() {
    }

    /**
     * 
     */
    private Integer id_;

    /**
     * 
     */
    private String nom_;

    /**
     * 
     */
    private String prenom_;
    
    /**
     * Parameterized constructor
     * @param id
     * @param nom
     * @param prenom
     */
    public Enseignant(Integer id,String nom,String prenom) {
    	id_=id;
    	nom_=nom;
    	prenom_=prenom;
    }
    
    /**
     * 
     * @return
     */
    public Integer getI() {
    	return id_;
    }
    
    /**
     * 
     * @return
     */
    public String getN() {
    	return nom_;
    }
    
    /**
     * 
     * @return
     */
    public String getP() {
    	return prenom_;
    }



}