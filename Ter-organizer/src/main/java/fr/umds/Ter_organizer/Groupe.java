package fr.umds.Ter_organizer;
import java.util.*;

/**
 * 
 */
public class Groupe {

    /**
     * Default constructor
     */
    public Groupe() {
    }

    /**
     * 
     */
    private Integer iD_;

    /**
     * 
     */
    private Sujet[] voeux_;

    /**
     * 
     */
    private List<Etudiant> etu_;

    /**
     * 
     */
    private Boolean affecter_=false;

    
    /**
     * 
     * @param iD
     * @param etu
     * @throws Exception
     */
    public Groupe(Integer iD,List<Etudiant> etu) throws Exception {
        if (etu.size()>6){
            throw new Exception();
        }
    	iD_=iD;
    	etu_=etu;
    }


    /**
     * @param voeux: 5 Sujets
     */
    public void setV(Sujet[] voeux) {
    	voeux_=voeux;
    }
    
    /**
     * 
     * @param val
     */
    public void setAf(Boolean val) {
    	affecter_=val;
    }
    
    /**
     * 
     * @return
     */
    public Integer getI() {
    	return iD_;
    }
    
    /**
     * 
     * @return
     */
    public Sujet[] getV() {
        return voeux_;
    }

    /**
     * 
     * @return
     */
    public List<Etudiant> getEtu() {
        return etu_;
    }
    
    /**
     * 
     * @return
     */
    public Boolean gethAf() {
    	return affecter_;
    	
    }
    	

  }