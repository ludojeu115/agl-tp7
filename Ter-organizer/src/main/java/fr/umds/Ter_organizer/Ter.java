package fr.umds.Ter_organizer;

import java.util.*;

/**
 * 
 */
public class Ter implements Hongrois {

    /**
     * Default constructor
     */
    public Ter() {
    }

    /**
     * 
     */
    private List<Sujet> sujets_;

    /**
     * 
     */
    private List<Groupe> groupes_;

    /**
     * 
     */
    private Planning planning_;

    /**
     * @param sujets
     */
    public void setSj(List<Sujet> sujets) {
        sujets_ = sujets;
        hauteur_ = sujets.size();
    }

    /**
     * @param groupes
     */
    public void setGr(List<Groupe> groupes) {
        groupes_ = groupes;
        largeur_ = groupes.size();
    }

    /**
     * 
     */
    public void affectGr() {
        List<List<Integer>> adjListe = new LinkedList<List<Integer>>();
        for (Integer gr = 0; gr < groupes_.size(); gr++) {
            for (Integer v = 0; v < 5; v++) {
                for (Integer s = 0; s < sujets_.size(); s++) {
                    if (sujets_.get(s) == groupes_.get(gr).getV()[v]) {
                        adjListe.add(Arrays.asList(s, gr, v));
                        break;
                    }
                }

            }
        }
        setAdjacenceList(adjListe);

        List<List<Integer>> affectL = affectation(1);
        for (List<Integer> e : affectL)
            sujets_.get(e.get(0)).setGr(groupes_.get(e.get(1)));

        affectL = affectation(2);
        for (List<Integer> e : affectL)
            sujets_.get(e.get(0)).setGr(groupes_.get(e.get(1)));

    }

    /**
     * 
     */
    public void affectRap() {
        // TODO implement here
    }

    /**
     * 
     */
    public void affectCr() {
        // TODO implement here
    }

    /**
     * 
     */
    public void generatePlanning() {
        planning_ = new Planning(sujets_);
    }

    public Planning getPl() {
        return planning_;
    }

    /** Hongrois */

    private int hauteur_;
    private int largeur_;
    private List<List<Integer>> adjListe_;

    public void setHauteur(int hauteur) {
        hauteur_ = hauteur;
    }

    public void setLargeur(int largeur) {
        largeur_ = largeur;

    }

    public void setAdjacenceList(List<List<Integer>> adjListe) {
        adjListe_ = adjListe;

    }

    public List<List<Integer>> affectation(int phaseNumber) {
        // TODO Auto-generated method stub
        return null;
    }

}